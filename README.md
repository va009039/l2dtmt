#l2dtmt

Live2Dアニメ製作乙女団 - キャラクターをLive2Dで動かしています。

##ファイル説明

- GAE - ホームページ(http://l2dtmt.appspot.com)のソースコード群。
- 拡張子cmo3 - Live2Dモデル(Live2D Cubism Editor)
- 拡張子clip - 素材分け(CLIP STUDIO PAINT)


ホームページはSDKのサンプルコードを使用しています http://sites.cybernoids.jp/cubism-sdk2/webgl2-1

キャラクター制作：遊佐めぐみ https://www.pixiv.net/fanbox/creator/20787425

